package ua.com.epam.entities;

import org.junit.jupiter.api.*;

import javax.xml.ws.WebServiceException;
import java.math.BigInteger;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static ua.com.epam.entities.AuthorFixture.*;

public class AuthorTest {

    static LibraryPortService service;
    static LibraryPort library;

    @BeforeAll
    static void setup() {
        service = new LibraryPortService();
        library = service.getLibraryPortSoap11();
    }

    @Test
    @DisplayName("GIVEN valid author " +
            "WHEN POST author " +
            "THEN return valid response")
    public void addAuthorPositive() {
        // GIVEN
        CreateAuthorRequest request = new CreateAuthorRequest();
        AuthorType newAuthor = createAuthor(getRandomId());
        request.setAuthor(newAuthor);

        // WHEN
        CreateAuthorResponse response = library.createAuthor(request);
        AuthorType createdAuthor = response.getAuthor();

        // THEN
        assertThat(createdAuthor).usingRecursiveComparison()
                .isEqualTo(newAuthor);
    }

    @Test
    @DisplayName("GIVEN existing author " +
            "WHEN POST author " +
            "THEN return valid response")
    public void addExistingAuthorNegative() {
        /// GIVEN
        String expectedErrorMessage = "Author with id " + AUTHOR_ID + " already exists";
        CreateAuthorRequest request = new CreateAuthorRequest();
        AuthorType newAuthor = createAuthor(AUTHOR_ID);
        request.setAuthor(newAuthor);

        // WHEN
        Throwable exception = catchThrowable(() -> library.createAuthor(request));

        // THEN
        assertThat(exception)
                .isInstanceOf(WebServiceException.class);
        assertThat(exception.getMessage())
                .contains(expectedErrorMessage);
    }

    @Test
    @DisplayName("GIVEN valid request " +
            "WHEN GET authors " +
            "THEN return valid response")
    public void getAllPositive() {
        // GIVEN
        int pageSize = 5;

        GetAuthorsRequest request = new GetAuthorsRequest();
        SearchParamsWithPagination search = new SearchParamsWithPagination();
        search.setSize(pageSize);
        search.setOrderType("desc");
        search.setPagination(true);
        search.setPage(BigInteger.ONE);
        request.setSearch(search);

        // WHEN
        GetAuthorsResponse response = library.getAuthors(request);
        List<AuthorType> authors = response.getAuthors().getAuthor();

        // THEN
        assertThat(authors)
                .isNotEmpty()
                .hasSize(pageSize)
                .hasOnlyElementsOfType(AuthorType.class);
    }

    @Test
    @DisplayName("GIVEN valid authorId " +
            "WHEN GET author " +
            "THEN return valid response")
    public void getByIdPositive() {
        AuthorType expectedAuthor = createAuthor(AUTHOR_ID);

        GetAuthorRequest request = new GetAuthorRequest();
        request.setAuthorId(AUTHOR_ID);

        // WHEN
        GetAuthorResponse response = library.getAuthor(request);
        AuthorType actualResponse = response.getAuthor();

        // THEN
        assertThat(actualResponse).usingRecursiveComparison()
                .isEqualTo(expectedAuthor);
    }

    @Test
    @DisplayName("GIVEN invalid authorId " +
            "WHEN GET author " +
            "THEN return valid response")
    public void getByIdNegative() {
        // GIVEN
        int authorId = 9999;
        String expectedErrorMessage = "Author with id " + authorId + " not found";
        GetAuthorRequest request = new GetAuthorRequest();
        request.setAuthorId(authorId);

        // WHEN
        Throwable exception = catchThrowable(() -> library.getAuthor(request));

        // THEN
        assertThat(exception)
                .isInstanceOf(WebServiceException.class);
        assertThat(exception.getMessage())
                .contains(expectedErrorMessage);
    }

}
