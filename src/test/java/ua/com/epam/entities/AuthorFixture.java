package ua.com.epam.entities;

import javax.xml.bind.annotation.XmlElement;
import java.util.Random;

class AuthorFixture {

    static final int AUTHOR_ID = 8272;
    static final String FIRST_NAME = "Otha";
    static final String SECOND_NAME = "Flatley";
    static final String BIRTH_CITY = "Rigobertoberg";
    static final String BIRTH_COUNTRY = "Kyrgyz Republic";
    static final String BIRTH_DATE = "1982-07-18";
    static final String AUTHOR_DESCR = "Dolorem eos dicta explicabo aut animi. Alias sit sit. Accusantium quisquam omnis repudiandae aut maiores numquam aperiam. Natus ut est explicabo architecto iure.";
    static final String NATIONALITY = "Albanian";

    static AuthorType.AuthorName createAuthorName(String first, String second) {
        AuthorType.AuthorName name = new AuthorType.AuthorName();
        name.setFirst(first);
        name.setSecond(second);

        return name;
    }

    static AuthorType.Birth createBirth(String date, String country, String city) {
        AuthorType.Birth birth = new AuthorType.Birth();
        birth.setCountry(country);
        birth.setCity(city);
        birth.setDate(date);

        return birth;
    }

    static AuthorType createAuthor(int authorId) {
        AuthorType author = new AuthorType();
        author.setAuthorId(authorId);
        author.setAuthorName(createAuthorName(FIRST_NAME, SECOND_NAME));
        author.setBirth(createBirth(BIRTH_DATE, BIRTH_COUNTRY, BIRTH_CITY));
        author.setAuthorDescription(AUTHOR_DESCR);
        author.setNationality(NATIONALITY);

        return author;
    }

    static int getRandomId() {
        return new Random()
                .ints(1, 1, 9999)
                .findFirst()
                .getAsInt();
    }

}
